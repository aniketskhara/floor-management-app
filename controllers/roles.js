const express = require("express");
const mongoose = require("mongoose");
const HttpError = require("../models/http-error");
const router = express.Router();
const { validationResult } = require("express-validator");

// importing models
const RoleModel = require("../models/role");
const RoleManagementModel = require("../models/roleManagement");

const getRoles = async (req, res) => {
  let roles, rolesCount;
  try {
    roles = await RoleModel.find();
    rolesCount = await RoleModel.countDocuments(roles);
  } catch (err) {
    return next(res.status(400).send({ error: err.message }));
  }
  res.json({ roles: roles, rolesCount: rolesCount });
};

const createRole = async (req, res, next) => {
  const { name } = req.body;

  const role = new RoleModel({
    name
  });

  try {
    await role.save();
    const roleManagement = new RoleManagementModel({
      role_id: role._id
    });
    await roleManagement.save();
    res.json({ message: "Role created Successfully" });
  } catch (err) {
    return next(res.status(422).send({ error: err.message }));
  }

  res.status(201).json({ role: role });
};

const getRoleById = async (req, res, next) => {
  const id = req.params.id;
  let role;

  try {
    role = await RoleModel.findById(id);
  } catch (err) {
    return next(res.status(404).send({ error: err.message }));
  }

  if (!role) {
    return next(res.status(400).send({ error: err.message }));
  } else {
    res.json({ role: role.toObject({ getters: true }) });
  }
};

const updateRole = async (req, res, next) => {
  const id = req.params.id;

  const { name } = req.body;
  let updatedRole;
  try {
    updatedRole = await RoleModel.findById(id);
  } catch (err) {
    return next(res.status(404).send({ error: err.message }));
    return next(error);
  }

  updatedRole.name = name;

  try {
    await updatedRole.save();
  } catch {
    return next(res.status(400).send({ error: err.message }));
  }

  res
    .status(200)
    .json({ updatedRole: updatedRole.toObject({ getters: true }) });
};

const deleteRole = async (req, res, next) => {
  const id = req.params.id;

  let deletedRole;
  try {
    deletedRole = await RoleModel.findById(id);
    deleteRoleManagement = await RoleManagementModel.findOne({ role_id: id });
  } catch (err) {
    return next(res.status(404).send({ error: err.message }));
  }

  if (!deletedRole) {
    return next(res.status(400).send({ error: err.message }));
  } else {
    try {
      await deletedRole.remove();
      await deleteRoleManagement.remove();
      res.json({ message: "Role deleted successfully" });
    } catch (err) {
      return next(res.status(422).send({ error: err.message }));
    }
  }
  res.status(200).json({ message: "Deleted successfully!" });
};

exports.getRoles = getRoles;
exports.createRole = createRole;
exports.getRoleById = getRoleById;
exports.deleteRole = deleteRole;
exports.updateRole = updateRole;
