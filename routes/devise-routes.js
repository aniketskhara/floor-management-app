const express = require("express");
const { check } = require("express-validator");
const DeviseController = require("../controllers/devises");
const HttpError = require("../models/http-error");
const router = express.Router();
const validateToken = require("../utils/utils").validateToken;
const mongoose = require("mongoose");

/**
 * @swagger
 * /devises:
 *   get:
 *     tags:
 *       - Devise
 *     description: Returns all devises
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of devices
 */
router.get("/", validateToken, DeviseController.getDevises);

/**
 * @swagger
 * /devises:
 *   post:
 *     tags:
 *       - Devise
 *     description: Saves a devise
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/Devise'
 *           type: object
 *           properties:
 *             model_no:
 *               type: string
 *             name:
 *               type: string
 *
 *         required:
 *           - model_no
 *           - name
 *     responses:
 *       201:
 *         description: New Devise created successfully
 */
router.post(
  "/",
  validateToken,
  [
    check("model_no")
      .not()
      .isEmpty(),
    check("name")
      .not()
      .isEmpty()
  ],
  DeviseController.createDevise
);

/**
 * @swagger
 * /devises/{id}:
 *   get:
 *     tags:
 *       - Devise
 *     description: Returns Object of single devise
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: Devise id
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Devise Object
 */
router.get("/:id", validateToken, DeviseController.getDeviseById);

/**
 * @swagger
 * /devises/{id}:
 *   delete:
 *     tags:
 *       - Devise
 *     description: Deletes a single devise
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: Devise id
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Devise Deleted Successfully
 */
router.delete("/:id", validateToken, DeviseController.deleteDevise);

/**
 * @swagger
 * /devises/{id}:
 *   patch:
 *     tags:
 *       - Devise
 *     description: Updates a single devise
 *     produces: application/json
 *     parameters:
 *       - name: id
 *         description: Devise id
 *         in: path
 *         required: true
 *         type: string
 *       - name: devise
 *         in: body
 *         description: Fields for the devise resource
 *     responses:
 *       200:
 *         description: Devise updated successfully
 */
router.patch(
  "/:id",
  validateToken,
  [
    check("model_no")
      .not()
      .isEmpty(),
    check("name")
      .not()
      .isEmpty()
  ],
  DeviseController.updateDevise
);

module.exports = router;
