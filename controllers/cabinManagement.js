const express = require("express");
const mongoose = require("mongoose");
const HttpError = require("../models/http-error");
const router = express.Router();
const { validationResult } = require("express-validator");

// importing models
const CabinManagementModel = require("../models/cabinManagement");

const getcabins = async (req, res) => {
  let CabinWithAccess;
  try {
    CabinWithAccess = await CabinManagementModel.find().populate(
      "cabin_id",
      "employee_id"
    );
  } catch (err) {
    const error = new HttpError("Something went wrong", 500);
    return next(error);
  }
  res.json({ CabinWithAccess: CabinWithAccess });
};

const createCabinAcccess = async (req, res, next) => {
  const { resourceId, start, end, title, employee_id } = req.body;

  const CabinWithAccess = new CabinManagementModel({
    resourceId,
    start,
    end,
    title,
    employee_id
  });

  try {
    await CabinWithAccess.save();
    res.json({ CabinWithAccess: CabinWithAccess, message: "Cabin Booked" });
  } catch (err) {
    console.log(err);
    const error = new HttpError("Cannot complete the action", 422);
    return next(error);
  }

  res.status(201).json({ CabinWithAccess: CabinWithAccess });
};

const getCabinAccessById = async (req, res, next) => {
  const id = req.params.id;
  let CabinWithAccess;

  try {
    CabinWithAccess = await CabinManagementModel.findById(id);
  } catch (err) {
    const error = new HttpError("Unable to find cabin access record.", 404);
    return next(error);
  }

  if (!CabinWithAccess) {
    return next(new HttpError("No record found.", 404));
  } else {
    res.json({ CabinWithAccess: CabinWithAccess.toObject({ getters: true }) });
  }
};

exports.getcabins = getcabins;
exports.createCabinAcccess = createCabinAcccess;
exports.getCabinAccessById = getCabinAccessById;
