const express = require("express");
const { check } = require("express-validator");
const DepartmentsController = require("../controllers/departments");
const HttpError = require("../models/http-error");
const router = express.Router();
const validateToken = require("../utils/utils").validateToken;
const mongoose = require("mongoose");

// importing models
// const EmployeeModel = mongoose.model("Employee");

/**
 * @swagger
 * /departments:
 *   get:
 *     tags:
 *       - Deapartments
 *     description: Returns all departments
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of roles
 */
router.get("/", validateToken, DepartmentsController.getDepartments);

/**
 * @swagger
 * /departments:
 *   post:
 *     tags:
 *       - Departments
 *     description: Create a department
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/Department'
 *           type: object
 *           properties:
 *             name:
 *               type: string
 *
 *         required:
 *           - name
 *     responses:
 *       201:
 *         description: New Department created successfully
 */
router.post(
  "/",
  validateToken,
  [
    check("name")
      .not()
      .isEmpty()
  ],
  DepartmentsController.createDepartment
);

/**
 * @swagger
 * /departments/{id}:
 *   get:
 *     tags:
 *       - Departments
 *     description: Returns a single department
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: department id
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: department
 */
router.get("/:id", validateToken, DepartmentsController.getDepartmentById);

/**
 * @swagger
 * /departments/{id}:
 *   delete:
 *     tags:
 *       - Departments
 *     description: Deletes a single department
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: Department id
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Department Deleted Successfully
 */
router.delete("/:id", validateToken, DepartmentsController.deleteDepartment);

/**
 * @swagger
 * /departments/{id}:
 *   patch:
 *     tags:
 *       - Departments
 *     description: Updates a single department
 *     produces: application/json
 *     parameters:
 *       - name: id
 *         description: Department id
 *         in: path
 *         required: true
 *         type: string
 *       - name: department
 *         in: body
 *         description: Fields for the department resource
 *     responses:
 *       200:
 *         description: Department updated successfully
 */
router.patch(
  "/:id",
  validateToken,
  [
    check("name")
      .not()
      .isEmpty()
  ],
  DepartmentsController.updateDepartment
);

module.exports = router;
