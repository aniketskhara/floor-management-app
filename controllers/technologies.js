const express = require("express");
const mongoose = require("mongoose");
const HttpError = require("../models/http-error");
const router = express.Router();
const { validationResult } = require("express-validator");

// importing models
const TechnologyModel = require("../models/technology");

const getTechnologies = async (req, res) => {
  let technologies, technologiesCount;
  try {
    technologies = await TechnologyModel.find();
    technologiesCount = await TechnologyModel.countDocuments(technologies);
  } catch (err) {
    return next(
      res.status(404).send({ error: "Unable to fetch the technologies" })
    );
  }
  res.json({
    technologies: technologies,
    technologiesCount: technologiesCount
  });
};

const createTechnology = async (req, res, next) => {
  const { name } = req.body;

  const technology = new TechnologyModel({
    name
  });

  try {
    await technology.save();
    res.json({ message: "Technology created Successfully" });
  } catch (err) {
    return next(res.status(400).send({ error: err.message }));
  }

  res.status(201).json({ technology: technology });
};

const getTechnologyById = async (req, res, next) => {
  const id = req.params.id;
  let technology;

  try {
    technology = await TechnologyModel.findById(id);
  } catch (err) {
    return next(res.status(404).send({ error: err.message }));
  }

  if (!technology) {
    return next(res.status(400).send({ error: err.message }));
  } else {
    res.json({ technology: technology.toObject({ getters: true }) });
  }
};

const updateTechnology = async (req, res, next) => {
  const id = req.params.id;

  const { name } = req.body;

  let updatedTechnology;
  try {
    updatedTechnology = await TechnologyModel.findById(id);
  } catch (err) {
    return next(res.status(404).send({ error: err.message }));
  }

  updatedTechnology.name = name;

  try {
    await updatedTechnology.save();
  } catch (err) {
    return next(res.status(400).send({ error: err.message }));
  }

  res
    .status(200)
    .json({ updatedTechnology: updatedTechnology.toObject({ getters: true }) });
};

const deleteTechnology = async (req, res, next) => {
  const id = req.params.id;

  let deletedTechnology;
  try {
    deletedTechnology = TechnologyModel.findById(id);
  } catch (err) {
    return next(res.status(404).send({ error: err.message }));
  }

  if (!deletedTechnology) {
    return next(res.status(400).send({ error: err.message }));
  } else {
    try {
      await deletedTechnology.remove();
      res.json({ message: "Technology deleted successfully" });
    } catch (err) {
      return next(res.status(400).send({ error: err.message }));
    }
  }
};

exports.getTechnologies = getTechnologies;
exports.createTechnology = createTechnology;
exports.getTechnologyById = getTechnologyById;
exports.deleteTechnology = deleteTechnology;
exports.updateTechnology = updateTechnology;
