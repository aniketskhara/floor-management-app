const express = require("express");
const { check } = require("express-validator");
const RolesController = require("../controllers/roles");
const HttpError = require("../models/http-error");
const router = express.Router();
const validateToken = require("../utils/utils").validateToken;
const mongoose = require("mongoose");

// importing models
// const EmployeeModel = mongoose.model("Employee");

/**
 * @swagger
 * /roles:
 *   get:
 *     tags:
 *       - Roles
 *     description: Returns all roles
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of roles
 */
router.get("/", validateToken, RolesController.getRoles);

/**
 * @swagger
 * /roles:
 *   post:
 *     tags:
 *       - Roles
 *     description: Saves a role
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/Role'
 *           type: object
 *           properties:
 *             name:
 *               type: string
 *
 *         required:
 *           - name
 *     responses:
 *       201:
 *         description: New Role created successfully
 */
router.post(
  "/",
  validateToken,
  [
    check("name")
      .not()
      .isEmpty()
  ],
  RolesController.createRole
);

/**
 * @swagger
 * /roles/{id}:
 *   get:
 *     tags:
 *       - Roles
 *     description: Returns Object of single role
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: Role id
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Role Object
 */
router.get("/:id", validateToken, RolesController.getRoleById);

/**
 * @swagger
 * /roles/{id}:
 *   delete:
 *     tags:
 *       - Roles
 *     description: Deletes a single role
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: Role id
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Role Deleted Successfully
 */
router.delete("/:id", validateToken, RolesController.deleteRole);

/**
 * @swagger
 * /roles/{id}:
 *   patch:
 *     tags:
 *       - Roles
 *     description: Updates a single role
 *     produces: application/json
 *     parameters:
 *       - name: id
 *         description: Role id
 *         in: path
 *         required: true
 *         type: string
 *       - name: role
 *         in: body
 *         description: Fields for the role resource
 *     responses:
 *       200:
 *         description: Role updated successfully
 */
router.patch(
  "/:id",
  validateToken,
  [
    check("name")
      .not()
      .isEmpty()
  ],
  RolesController.updateRole
);

module.exports = router;
