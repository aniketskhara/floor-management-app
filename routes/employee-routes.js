const express = require("express");
const { check } = require("express-validator");
const EmployeeController = require("../controllers/employees");
const HttpError = require("../models/http-error");
const router = express.Router();
const validateToken = require("../utils/utils").validateToken;
const mongoose = require("mongoose");

/**
 * @swagger
 * /employees:
 *   get:
 *     tags:
 *       - Employees
 *     description: Returns all employees
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of employees
 */
router.get("/", validateToken, EmployeeController.getEmployee);

/**
 * @swagger
 * /employees:
 *   post:
 *     tags:
 *       - Employees
 *     description: Saves an employee
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/Employee'
 *           type: object
 *           properties:
 *             firstName:
 *               type: string
 *             lastName:
 *               type: string
 *             email:
 *               type: string
 *
 *         required:
 *           - firstName
 *           - lastName
 *           - email
 *     responses:
 *       201:
 *         description: New Employee created successfully
 */
router.post(
  "/",
  validateToken,
  [
    check("email")
      .not()
      .isEmpty(),
    check("password").isLength({ min: 6 }),
    check("contactNumber")
      .isLength({ max: 10, min: 10 })
      .isNumeric(),
    check("firstName")
      .not()
      .isEmpty(),
    check("lastName")
      .not()
      .isEmpty(),
    check("skype_id")
      .not()
      .isEmpty(),
    check("experience")
      .not()
      .isEmpty()
  ],
  EmployeeController.createEmployee
);

/**
 * @swagger
 * /employees/{id}:
 *   get:
 *     tags:
 *       - Employees
 *     description: Returns Object of single user
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: Employee's id
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: User Object
 */
router.get("/:id", validateToken, EmployeeController.getEmployeeById);

/**
 * @swagger
 * /employees/{id}:
 *   delete:
 *     tags:
 *       - Employees
 *     description: Deletes a single employee
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: Employee's id
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Employee Deleted Successfully
 */
router.delete("/:id", validateToken, EmployeeController.deleteEmployee);

/**
 * @swagger
 * /employees/{id}:
 *   patch:
 *     tags:
 *       - Employees
 *     description: Updates a single employee
 *     produces: application/json
 *     parameters:
 *       - name: id
 *         description: Employee's id
 *         in: path
 *         required: true
 *         type: string
 *       - name: employee
 *         in: body
 *         description: Fields for the employee resource
 *     responses:
 *       200:
 *         description: Employee updated successfully
 */
router.patch(
  "/:id",
  validateToken,
  [
    check("firstName")
      .not()
      .isEmpty(),
    check("lastName")
      .not()
      .isEmpty(),
    check("contactNumber")
      .isLength({ max: 10, min: 10 })
      .isNumeric(),
    check("firstName")
      .not()
      .isEmpty(),
    check("lastName")
      .not()
      .isEmpty(),
    check("skype_id")
      .not()
      .isEmpty(),
    check("experience")
      .not()
      .isEmpty()
  ],
  EmployeeController.updateEmployee
);

router.post(
  "/login",
  check("email")
    .not()
    .isEmpty(),
  check("password").isLength({ min: 6 }),
  EmployeeController.Login
);

router.post(
  "/update-password",
  validateToken,
  check("password").isLength({ min: 6 }),
  check("new_password").isLength({ min: 6 }),
  EmployeeController.UpdatePassword
);
module.exports = router;
