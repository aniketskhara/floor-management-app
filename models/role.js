const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");

var roleSchema = new mongoose.Schema({
  name: {
    type: String,
    unique: true,
    required: true
  }
});

roleSchema.plugin(uniqueValidator);
module.exports = mongoose.model("Role", roleSchema);
