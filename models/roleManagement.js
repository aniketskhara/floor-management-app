const mongoose = require("mongoose");

var roleManagementSchema = new mongoose.Schema({
  role_id: {
    type: mongoose.Types.ObjectId,
    required: [true, "Role cannot be blank"],
    ref: "Role"
  },
  access: []
});

module.exports = mongoose.model("RoleManagement", roleManagementSchema);
