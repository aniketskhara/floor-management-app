const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
const bcrypt = require("bcrypt");
const stage = require("../config/config");

var employeeSchema = new mongoose.Schema({
  email: {
    type: String,
    unique: [true, "Email already exists"],
    required: [true, "Email cannot be blank"],
    trim: true
  },
  firstName: {
    type: String,
    required: [true, "First Name cannot be blank"]
  },
  lastName: {
    type: String,
    required: [true, "Last Name cannot be blank"]
  },
  skype_id: {
    type: String
  },
  contactNumber: {
    type: String,
    required: [true, "Contact Number cannot be blank"]
  },
  experience: {
    type: String
  },
  password: {
    type: String,
    required: [true, "Password cannot be blank"],
    trim: true
  },
  token: {
    type: String
  },
  // Need to change it from array to string
  role: {
    type: mongoose.Types.ObjectId,
    required: [true, "Role cannot be blank"],
    ref: "Role"
  },
  department: {
    type: mongoose.Schema.Types.ObjectId,
    required: [true, "Department cannot be blank"],
    ref: "Department"
  },
  // department: {
  //   type: String
  // },
  courses: [],
  techonologies: [],
  cabin: [],
  devise: []
});

employeeSchema.pre("save", function(next) {
  const user = this;
  if (!user.isModified || !user.isNew) {
    // don't rehash if it's an old user
    next();
  } else {
    bcrypt.hash(user.password, stage.saltingRounds, function(err, hash) {
      if (err) {
        console.log("Error hashing password for user", user.email);
        next(err);
      } else {
        user.password = hash;
        next();
      }
    });
  }
});

employeeSchema.plugin(uniqueValidator);
module.exports = mongoose.model("Employee", employeeSchema);
