const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");

var departmentSchema = new mongoose.Schema({
  name: {
    type: String,
    unique: true,
    required: true
  }
});

departmentSchema.plugin(uniqueValidator);
module.exports = mongoose.model("Department", departmentSchema);
