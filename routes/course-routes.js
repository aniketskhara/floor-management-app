const express = require("express");
const { check } = require("express-validator");
const CoursesController = require("../controllers/courses");
const HttpError = require("../models/http-error");
const router = express.Router();
const validateToken = require("../utils/utils").validateToken;

const mongoose = require("mongoose");

/**
 * @swagger
 * /courses:
 *   get:
 *     tags:
 *       - Course
 *     description: Returns all courses
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of courses
 */
router.get("/", validateToken, CoursesController.getCourses);

/**
 * @swagger
 * /courses:
 *   post:
 *     tags:
 *       - Course
 *     description: Saves a course
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/Course'
 *           type: object
 *           properties:
 *             name:
 *               type: string
 *             type: array
 *             links:
 *                type: object
 *         required:
 *           - name
 *     responses:
 *       201:
 *         description: New Course created successfully
 */
router.post(
  "/",
  validateToken,
  [
    check("name")
      .not()
      .isEmpty()
  ],
  CoursesController.createCourse
);

/**
 * @swagger
 * /courses/{id}:
 *   get:
 *     tags:
 *       - Course
 *     description: Returns Object of single course
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: Course id
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Devise Object
 */
router.get("/:id", validateToken, CoursesController.getCourseById);

/**
 * @swagger
 * /courses/{id}:
 *   delete:
 *     tags:
 *       - Course
 *     description: Deletes a single course
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: Course id
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Course Deleted Successfully
 */
router.delete("/:id", validateToken, CoursesController.deleteCourse);

/**
 * @swagger
 * /courses/{id}:
 *   patch:
 *     tags:
 *       - Course
 *     description: Updates a single course
 *     produces: application/json
 *     parameters:
 *       - name: id
 *         description: Course id
 *         in: path
 *         required: true
 *         type: string
 *       - name: devise
 *         in: body
 *         description: Fields for the course resource
 *     responses:
 *       200:
 *         description: Course updated successfully
 */
router.patch(
  "/:id",
  validateToken,
  [
    check("name")
      .not()
      .isEmpty()
  ],
  CoursesController.updateCourse
);

module.exports = router;
