const mongoose = require("mongoose");

var courseSchema = new mongoose.Schema({
  name: {
    type: String
  },
  links: []
});

module.exports = mongoose.model("Course", courseSchema);
