const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
var technologySchema = new mongoose.Schema({
  name: {
    type: String,
    unique: true,
    required: true
  }
});

technologySchema.plugin(uniqueValidator);
module.exports = mongoose.model("Technology", technologySchema);
