const mongoose = require("mongoose");

var osSchema = new mongoose.Schema({
  name: {
    type: String
  }
});

module.exports = mongoose.model("Os", osSchema);
