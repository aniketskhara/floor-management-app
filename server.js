const express = require("express");
require("dotenv").config(); // Sets up dotenv as soon as our application starts

const app = express();
const mongoose = require("mongoose");
const cors = require("cors");
const bodyParser = require("body-parser");
const os = require("os");
const YAML = require("yamljs");
const config = require("./config/config");
const stage = require("./config/config");
// define the connection file
const db = require("./models/db");
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");
// define routes
const employeeRoutes = require("./routes/employee-routes");
const roleRoutes = require("./routes/role-routes");
const courseRoutes = require("./routes/course-routes");
const technologyRoutes = require("./routes/technology-routes");
const cabinRoutes = require("./routes/cabin-routes");
const deviseRoutes = require("./routes/devise-routes");
const departmentRoutes = require("./routes/department-routes");
const osRoutes = require("./routes/os-routes");
const roleManagementRoutes = require("./routes/role-management-routes");
const cabinManagementRoutes = require("./routes/cabin-management-routes");
const deviseManagementRoutes = require("./routes/devise-management-routes");

const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: "Floor Management API",
      description: "Floor Management API Information",
      servers: ["http://localhost:8000"]
    }
  },
  apis: ["./routes/*.js"]
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));

app.use(bodyParser.json());

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, PATCH, DELETE");
  next();
});

app.set("case sensitive routing", true);
app.set("env", config.NODE_ENV);
app.set("port", config.PORT);

// Routes

app.get("/", (req, res) => {
  res.send("Root Page");
});

app.listen(8000, () => {
  console.log("Example app listening on port 8000!");
});

app.use("/employees", employeeRoutes);
app.use("/roles", roleRoutes);
app.use("/courses", courseRoutes);
app.use("/technologies", technologyRoutes);
app.use("/cabins", cabinRoutes);
app.use("/devises", deviseRoutes);
app.use("/departments", departmentRoutes);
app.use("/os", osRoutes);
app.use("//((?!login).)*/", employeeRoutes);
app.use("/role-management", roleManagementRoutes);
app.use("/cabin-management", cabinManagementRoutes);
app.use("/devise-management", deviseManagementRoutes);

const swaggerDocument = YAML.load("./api/swagger/swagger.yaml");

app.use(cors());

app.use(function(req, res) {
  return res.status(404).send({
    success: false,
    msg: "API not found",
    data: req.originalUrl + " not found"
  });
});

// this route is only for upload file testing using html code use uploadImage api to upload file
// app.get('/upload', function (req, res) {
//     res.sendFile(__dirname + '/partials/index.html');
// });

if (config.CLUSTERING) {
  startServer();
} else {
  startServer();
}

function startServer() {
  app.listen(app.get("port"), function() {
    console.log(
      `Server is listening on http://${os.hostname()}:${app.get("port")}`
    );
  });
}

app.use(function(err, req, res, next) {
  return res.status(500).send({
    success: false,
    msg: "Internal Server Error",
    data: app.get("env") === "production" ? {} : err.stack
  });
});

module.exports = app;
