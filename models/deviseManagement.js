const mongoose = require("mongoose");

var deviseManagementSchema = new mongoose.Schema({
  deviseId: {
    type: mongoose.Types.ObjectId,
    required: [true, "Devise cannot be blank"],
    ref: "Devise"
  },
  start: {
    type: Date,
    required: [true, "Start cannot be blank"]
  },
  end: {
    type: Date,
    required: [true, "End cannot be blank"]
  },
  title: {
    type: String,
    required: [true, "Title cannot be blank"]
  },
  employee_id: {
    type: mongoose.Types.ObjectId,
    required: [true, "Employee cannot be blank"],
    ref: "Employee"
  }
});

module.exports = mongoose.model("DeviseManagement", deviseManagementSchema);
