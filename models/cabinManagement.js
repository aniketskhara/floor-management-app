const mongoose = require("mongoose");

var cabinManagementSchema = new mongoose.Schema({
  resourceId: {
    type: mongoose.Types.ObjectId,
    required: [true, "Cabin cannot be blank"],
    ref: "Cabin"
  },
  start: {
    type: Date
  },
  end: {
    type: Date
  },
  title: {
    type: String
  },
  employee_id: {
    type: mongoose.Types.ObjectId,
    required: [true, "Employee cannot be blank"],
    ref: "Employee"
  }
});

module.exports = mongoose.model("CabinManagement", cabinManagementSchema);
