const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");

var cabinSchema = new mongoose.Schema({
  cabin_no: {
    type: String,
    unique: true,
    required: true
  }
});

cabinSchema.plugin(uniqueValidator);

module.exports = mongoose.model("Cabin", cabinSchema);
