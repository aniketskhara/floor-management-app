const express = require("express");
const mongoose = require("mongoose");
const HttpError = require("../models/http-error");
const router = express.Router();
const { validationResult } = require("express-validator");

// importing models
const CabinModel = require("../models/cabin");

const getCabins = async (req, res, next) => {
  let cabins, cabinsCount;

  try {
    cabins = await CabinModel.find();
    cabinsCount = await CabinModel.countDocuments(cabins);
  } catch (err) {
    return next(res.status(400).send({ error: err.message }));
  }
  res.json({ cabins: cabins, cabinsCount: cabinsCount });
};

const createCabin = async (req, res, next) => {
  const { cabin_no } = req.body;

  const cabin = new CabinModel({
    cabin_no
  });

  try {
    await cabin.save();
    res.json({ message: "Cabin created Successfully" });
  } catch (err) {
    return next(res.status(400).send({ error: err.message }));
  }

  res.status(201).json({ cabin: cabin });
};

const getCabinById = async (req, res, next) => {
  const id = req.params.id;
  let cabin;

  try {
    cabin = await CabinModel.findById(id);
  } catch (err) {
    return next(res.status(404).send({ error: err.message }));
  }

  if (!cabin) {
    return next(res.status(404).send({ error: err.message }));
  } else {
    res.json({ cabin: cabin.toObject({ getters: true }) });
  }
};

const updateCabin = async (req, res, next) => {
  // const errors = validationResult(req);
  // if (!errors.isEmpty()) {
  //   return next(new HttpError("Invalid inputs. Please check the inputs", 422));
  // }

  const id = req.params.id;

  const { cabin_no } = req.body;

  let updatedCabin;
  try {
    updatedCabin = await CabinModel.findById(id);
  } catch (err) {
    return next(res.status(400).send({ error: err.message }));
  }

  updatedCabin.cabin_no = cabin_no;

  try {
    await updatedCabin.save();
  } catch (err) {
    return next(res.status(400).send({ error: err.message }));
  }

  res
    .status(200)
    .json({ updatedCabin: updatedCabin.toObject({ getters: true }) });
};

const deleteCabin = async (req, res, next) => {
  const id = req.params.id;

  let deletedCabin;
  try {
    deletedCabin = await CabinModel.findById(id);
  } catch (err) {
    return next(res.status(404).send({ error: err.message }));
  }

  if (!deletedCabin) {
    return next(res.status(400).send({ error: err.message }));
  } else {
    try {
      await deletedCabin.remove();
      res.json({ message: "Cabin deleted successfully" });
    } catch (err) {
      return next(res.status(422).send({ error: err.message }));
    }
  }

  res.status(200).json({ message: "Cabin deleted successfully" });
};

exports.getCabins = getCabins;
exports.createCabin = createCabin;
exports.getCabinById = getCabinById;
exports.deleteCabin = deleteCabin;
exports.updateCabin = updateCabin;
