const mongoose = require("mongoose");

const db = process.env.MONGODB_URL;
// mongoose.connect('mongodb://localhost:27017/floor-management', { useNewUrlParser: true,
mongoose.connect(
  // "mongodb+srv://aniket:aniket@123456@cluster0-v1wvj.mongodb.net/floor-management?retryWrites=true&w=majority"
  db,
  { useNewUrlParser: true, useUnifiedTopology: true },
  err => {
    if (!err) {
      console.log("Mongo DB Connected Successfully");
    } else {
      console.log("Error in connection" + err);
    }
  }
);

const Employee = require("./user");
const Role = require("./role");
