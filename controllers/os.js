const express = require("express");
const mongoose = require("mongoose");
const HttpError = require("../models/http-error");
const router = express.Router();
const { validationResult } = require("express-validator");

// importing models
const OsModel = require("../models/os");

const getOs = async (req, res) => {
  let Os;
  try {
    Os = await OsModel.find();
  } catch (err) {
    const error = new HttpError("Something went wrong", 500);
    return next(error);
  }
  res.json({ Os: Os });
};

const createOs = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(new HttpError("Invalid inputs, please check the inputs", 422));
  }

  const { name } = req.body;

  const Os = new OsModel({
    name
  });

  try {
    await Os.save();
    res.json({ message: "Os created Successfully" });
  } catch (err) {
    const error = new HttpError("Cannot complete the action", 422);
    return next(error);
  }

  res.status(201).json({ Os: Os });
};

const getOsById = async (req, res, next) => {
  const id = req.params.id;
  let Os;

  try {
    Os = await OsModel.findById(id);
  } catch (err) {
    const error = new HttpError("Unable to delete.", 500);
    return next(error);
  }

  if (!Os) {
    return next(new HttpError("No record found.", 404));
  } else {
    res.json({ Os: Os.toObject({ getters: true }) });
  }
};

const updateOs = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(new HttpError("Invalid inputs, please check the inputs", 422));
  }

  const id = req.params.id;

  const { name } = req.body;
  let updatedOs;
  try {
    updatedOs = await OsModel.findById(id);
  } catch (err) {
    const error = new HttpError("Something went wrong", 500);
    return next(error);
  }

  updatedOs.name = name;

  try {
    await updatedOs.save();
  } catch {
    const error = new HttpError("Unable to update role.", 422);
    return next(error);
  }

  res
    .status(200)
    .json({ updatedOs: updatedOs.toObject({ getters: true }) });
};

const deleteOs = async (req, res, next) => {
  const id = req.params.id;

  let deletedOs;
  try {
    deletedOs = await OsModel.findById(id);
  } catch (err) {
    const error = new HttpError("Unable to update role.", 422);
    return next(error);
  }

  if (!deletedOs) {
    return next(new HttpError("something went wrong", 500));
  } else {
    try {
      await deletedOs.remove();
      res.json({ message: "OS deleted successfully" });
    } catch (err) {
      const error = new HttpError("cannot delete the role.", 422);
      return next(error);
    }
  }
  res.status(200).json({ message: "Deleted successfully!" });
};

exports.getOs = getOs;
exports.createOs = createOs;
exports.getOsById = getOsById;
exports.deleteOs = deleteOs;
exports.updateOs = updateOs;
