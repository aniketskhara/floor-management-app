const express = require("express");
const { check } = require("express-validator");
const deviseManagementController = require("../controllers/deviseManagement");
const HttpError = require("../models/http-error");
const router = express.Router();
const validateToken = require("../utils/utils").validateToken;
const mongoose = require("mongoose");

router.get("/", validateToken, deviseManagementController.getDeviseRecords);

router.post("/", validateToken, deviseManagementController.createDeviseEvent);

router.get("/:id", validateToken, deviseManagementController.getDeviseRecordById);

module.exports = router;
