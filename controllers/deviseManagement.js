const express = require("express");
const mongoose = require("mongoose");
const HttpError = require("../models/http-error");
const router = express.Router();
const { validationResult } = require("express-validator");

// importing models
const DeviseManagementModel = require("../models/deviseManagement");

const getDeviseRecords = async (req, res) => {
  let DeviseEvents;
  try {
    DeviseEvents = await DeviseManagementModel.find().populate("employee_id");
  } catch (err) {
    const error = new HttpError("Something went wrong", 500);
    return next(error);
  }  
  res.json({ DeviseEvents: DeviseEvents })
};

const createDeviseEvent =  async (req, res, next) => {
  const { deviseId, start, end, title, employee_id } = req.body;

  const newEvent = new DeviseManagementModel({
    deviseId,
    start,
    end,
    title,
    employee_id
  });

  if (deviseId != '') {
    try {
      await newEvent.save();
    } 
    catch (err) {
      return next(res.status(400).send({ err: err.message }));
    }
    res.status(201).json({ savedEvent: newEvent });
  }
  else {
    res.status(400).json({ err: "deviseId: Devise not selected" })
  }

};

const getDeviseRecordById = async (req, res, next) => {
  const id = req.params.id;
  let Event;

  try {
    Event = await DeviseManagementModel.findById(id);
  } catch (err) {
    const error = new HttpError("Unable to find cabin access record.", 404);
    return next(error);
  }

  if (!Event) {
    return next(new HttpError("No record found.", 404));
  } else {
    res.json({ Event: Event.toObject({ getters: true }) });
  }
};

exports.getDeviseRecords = getDeviseRecords;
exports.createDeviseEvent = createDeviseEvent;
exports.getDeviseRecordById = getDeviseRecordById;
