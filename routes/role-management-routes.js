const express = require("express");
const { check } = require("express-validator");
const roleManagementController = require("../controllers/roleManagement");
const HttpError = require("../models/http-error");
const router = express.Router();
const validateToken = require("../utils/utils").validateToken;
const mongoose = require("mongoose");

// importing models
// const EmployeeModel = mongoose.model("Employee");

/**
 * @swagger
 * /roles:
 *   get:
 *     tags:
 *       - Roles
 *     description: Returns all roles
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of roles
 */
router.get("/", validateToken, roleManagementController.getRolesWithAccess);

/**
 * @swagger
 * /roles:
 *   post:
 *     tags:
 *       - Roles
 *     description: Saves a role
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/Role'
 *           type: object
 *           properties:
 *             name:
 *               type: string
 *
 *         required:
 *           - name
 *     responses:
 *       201:
 *         description: New Role created successfully
 */
router.post(
  "/",
  validateToken,
  [
    check("role_id")
      .not()
      .isEmpty()
  ],
  roleManagementController.createRolesWithAccess
);

/**
 * @swagger
 * /roles/{id}:
 *   get:
 *     tags:
 *       - Roles
 *     description: Returns Object of single role
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: Role id
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Role Object
 */
router.get(
  "/:id",
  validateToken,
  roleManagementController.getRolesWithAccessById
);

router.get(
  "/role-item/:id",
  validateToken,
  roleManagementController.getAccessForSelectedRole
);

/**
 * @swagger
 * /roles/{id}:
 *   delete:
 *     tags:
 *       - Roles
 *     description: Deletes a single role
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: Role id
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Role Deleted Successfully
 */
router.delete(
  "/:id",
  validateToken,
  roleManagementController.deleteRolesWithAccess
);

/**
 * @swagger
 * /roles/{id}:
 *   patch:
 *     tags:
 *       - Roles
 *     description: Updates a single role
 *     produces: application/json
 *     parameters:
 *       - name: id
 *         description: Role id
 *         in: path
 *         required: true
 *         type: string
 *       - name: role
 *         in: body
 *         description: Fields for the role resource
 *     responses:
 *       200:
 *         description: Role updated successfully
 */
router.patch(
  "/:id",
  validateToken,
  [
    check("role_id")
      .not()
      .isEmpty()
  ],
  roleManagementController.updateRolesWithAccess
);

router.patch(
  "/role-item/:id",
  roleManagementController.updateAccessForSelectedRole
);

module.exports = router;
