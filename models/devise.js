const mongoose = require("mongoose");

var deviseSchema = new mongoose.Schema({
  model_no: {
    type: String
  },
  name: {
    type: String
  },
  os: {
    type: mongoose.Types.ObjectId,
    required: true,
    ref: "Os"
  }
});

module.exports = mongoose.model("Devise", deviseSchema);
