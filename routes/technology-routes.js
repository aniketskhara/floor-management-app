const express = require("express");
const { check } = require("express-validator");
const TechnologiesController = require("../controllers/technologies");
const HttpError = require("../models/http-error");
const router = express.Router();
const validateToken = require("../utils/utils").validateToken;
const mongoose = require("mongoose");

/**
 * @swagger
 * /technologies:
 *   get:
 *     tags:
 *       - Technologies
 *     description: Returns all technologies
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of technologies
 */
router.get("/", validateToken, TechnologiesController.getTechnologies);

/**
 * @swagger
 * /technologies:
 *   post:
 *     tags:
 *       - Technologies
 *     description: Saves a technology
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/Technology'
 *           type: object
 *           properties:
 *             name:
 *               type: string
 *
 *         required:
 *           - name
 *     responses:
 *       201:
 *         description: New Technology created successfully
 */
router.post(
  "/",
  validateToken,
  [
    check("name")
      .not()
      .isEmpty()
  ],
  TechnologiesController.createTechnology
);

/**
 * @swagger
 * /technologies/{id}:
 *   get:
 *     tags:
 *       - Technologies
 *     description: Returns Object of single technology
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: Technology id
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Technology Object
 */
router.get("/:id", validateToken, TechnologiesController.getTechnologyById);

/**
 * @swagger
 * /technologies/{id}:
 *   delete:
 *     tags:
 *       - Technologies
 *     description: Deletes a single technology
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: Technology id
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Technology Deleted Successfully
 */
router.delete("/:id", validateToken, TechnologiesController.deleteTechnology);

/**
 * @swagger
 * /technologies/{id}:
 *   patch:
 *     tags:
 *       - Technologies
 *     description: Updates a single technology
 *     produces: application/json
 *     parameters:
 *       - name: id
 *         description: Technology id
 *         in: path
 *         required: true
 *         type: string
 *       - name: role
 *         in: body
 *         description: Fields for the technology resource
 *     responses:
 *       200:
 *         description: Technology updated successfully
 */
router.patch(
  "/:id",
  validateToken,
  [
    check("name")
      .not()
      .isEmpty()
  ],
  TechnologiesController.updateTechnology
);

module.exports = router;
