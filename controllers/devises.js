const express = require("express");
const mongoose = require("mongoose");
const HttpError = require("../models/http-error");
const { validationResult } = require("express-validator");
const router = express.Router();

// importing models
const DeviseModel = require("../models/devise");

const getDevises = async (req, res) => {
  let devises, devisesCount;
  try {
    devises = await DeviseModel.find().populate("os");
    devisesCount = await DeviseModel.countDocuments(devises);
  } catch (err) {
    return next(res.status(404).send({ error: err.message }));
  }
  res.json({ devises: devises, devisesCount: devisesCount });
};

const createDevise = async (req, res, next) => {
  const { model_no, name, os } = req.body;

  const devise = new DeviseModel({
    model_no,
    name,
    os
  });

  try {
    await devise.save();
    res.json({ message: "Devise added Successfully" });
  } catch (err) {
    return next(res.status(400).send({ error: err.message }));
  }
  res.status(201).json({ devise: devise });
};

const getDeviseById = async (req, res, next) => {
  const id = req.params.id;
  let devise;

  try {
    devise = await DeviseModel.findById(id);
  } catch (err) {
    return next(res.status(404).send({ error: err.message }));
  }

  if (!devise) {
    return next(res.status(404).send({ error: err.message }));
  } else {
    res.json({ devise: devise.toObject({ getters: true }) });
  }
};

const updateDevise = async (req, res, next) => {
  const id = req.params.id;

  const { model_no, name, os } = req.body;

  let UpdatedDevise;
  try {
    UpdatedDevise = await DeviseModel.findById(id);
  } catch (err) {
    return next(res.status(404).send({ error: err.message }));
  }

  UpdatedDevise.model_no = model_no;
  UpdatedDevise.name = name;
  UpdatedDevise.os = os;

  try {
    await UpdatedDevise.save();
  } catch (err) {
    return next(res.status(400).send({ error: err.message }));
  }

  res
    .status(200)
    .json({ UpdatedDevise: UpdatedDevise.toObject({ getters: true }) });
};

const deleteDevise = async (req, res, next) => {
  const id = req.params.id;

  let deletedDevice;
  try {
    deletedDevice = DeviseModel.findById(id);
  } catch (err) {
    return next(res.status(404).send({ error: err.message }));
  }

  if (!deletedDevice) {
    return next(res.status(404).send({ error: err.message }));
  } else {
    try {
      await deletedDevice.remove();
      res.json({ message: "Device deleted successfully" });
    } catch (err) {
      return next(res.status(422).send({ error: err.message }));
    }
  }
};

exports.getDevises = getDevises;
exports.createDevise = createDevise;
exports.getDeviseById = getDeviseById;
exports.deleteDevise = deleteDevise;
exports.updateDevise = updateDevise;
