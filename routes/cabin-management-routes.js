const express = require("express");
const { check } = require("express-validator");
const cabinManagementController = require("../controllers/cabinManagement");
const HttpError = require("../models/http-error");
const router = express.Router();
const validateToken = require("../utils/utils").validateToken;
const mongoose = require("mongoose");

router.get("/", validateToken, cabinManagementController.getcabins);

router.post("/", validateToken, cabinManagementController.createCabinAcccess);

router.get("/:id", validateToken, cabinManagementController.getCabinAccessById);

module.exports = router;
