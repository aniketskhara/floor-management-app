const express = require("express");
const mongoose = require("mongoose");
const HttpError = require("../models/http-error");
const router = express.Router();
const { validationResult } = require("express-validator");

// importing models
const RoleManagementModel = require("../models/roleManagement");

const getRolesWithAccess = async (req, res) => {
  let RolesWithAccess;
  try {
    RolesWithAccess = await RoleManagementModel.find().populate("role_id");;
  } catch (err) {
    const error = new HttpError("Something went wrong", 500);
    return next(error);
  }
  res.json({ RolesWithAccess: RolesWithAccess });
};

const createRolesWithAccess = async (req, res, next) => {
  const { role_id, access } = req.body;

  const RolesWithAccess = new RoleManagementModel({
    role_id,
    access
  });

  try {
    await RolesWithAccess.save();
    res.json({ message: "Permissions Added" });
  } 
  catch (err) {
    const error = new HttpError("Cannot complete the action", 422);
    return next(error);
  }

  res.status(201).json({ RolesWithAccess: RolesWithAccess });
};

const getRolesWithAccessById = async (req, res, next) => {
  const id = req.params.id;
  let RolesWithAccess;

  try {
    RolesWithAccess = await RoleManagementModel.findById(id);
  } catch (err) {
    const error = new HttpError("Unable to delete.", 500);
    return next(error);
  }

  if (!RolesWithAccess) {
    return next(new HttpError("No record found.", 404));
  } else {
    res.json({ RolesWithAccess: RolesWithAccess.toObject({ getters: true }) });
  }
};

const updateRolesWithAccess = async (req, res, next) => {

  const id = req.params.id;

  const { role_id, access } = req.body;
  let updatedRolesWithAccess;
  try {
    updatedRolesWithAccess = await RoleManagementModel.findById(id);
  } catch (err) {
    const error = new HttpError("Something went wrong", 500);
    return next(error);
  }

  updatedRolesWithAccess.role_id = role_id;
  updatedRolesWithAccess.access = access;

  try {
    await updatedRolesWithAccess.save();
  } catch {
    const error = new HttpError("Unable to update role.", 422);
    return next(error);
  }

  res
    .status(200)
    .json({ updatedRolesWithAccess: updatedRolesWithAccess.toObject({ getters: true }) });
};

const deleteRolesWithAccess = async (req, res, next) => {
  const id = req.params.id;

  let deletedRolesWithAccess;
  try {
    deletedRolesWithAccess = await RoleManagementModel.findById(id);
  } catch (err) {
    const error = new HttpError("Unable to update role.", 422);
    return next(error);
  }

  if (!deletedRolesWithAccess) {
    return next(new HttpError("something went wrong", 500));
  } else {
    try {
      await deletedRolesWithAccess.remove();
      res.json({ message: "RolesWithAccess deleted successfully" });
    } catch (err) {
      const error = new HttpError("cannot delete the role.", 422);
      return next(error);
    }
  }
  res.status(200).json({ message: "RolesWithAccess Deleted successfully!" });
};

const getAccessForSelectedRole = async (req, res, next) => {
  const id = req.params.id;
  let RolesWithAccess;

  try {
    RolesWithAccess = await RoleManagementModel.findOne({ role_id: id });
  } catch (err) {
    const error = new HttpError("Unable to delete.", 500);
    return next(error);
  }

  if (!RolesWithAccess) {
    return next(new HttpError("No record found.", 404));
  } else {
    res.json({ RolesWithAccess: RolesWithAccess.toObject({ getters: true }) });
  }
};

const updateAccessForSelectedRole = async (req, res, next) => {
  const id = req.params.id;

  const { access } = req.body;
  let UpdatedAccess;

  try {
    UpdatedAccess = await RoleManagementModel.findOne({ role_id: id });
  } catch (err) {
    const error = new HttpError("Unable to delete.", 500);
    return next(error);
  }

  UpdatedAccess.role_id = id;
  UpdatedAccess.access = access;

  try {
    await UpdatedAccess.save();
  } catch {
    const error = new HttpError("Unable to update role.", 422);
    return next(error);
  }

  res
    .status(200)
    .json({ UpdatedAccess: UpdatedAccess.toObject({ getters: true }) });
};

exports.getRolesWithAccess = getRolesWithAccess;
exports.createRolesWithAccess = createRolesWithAccess;
exports.getRolesWithAccessById = getRolesWithAccessById;
exports.deleteRolesWithAccess = deleteRolesWithAccess;
exports.updateRolesWithAccess = updateRolesWithAccess;
exports.getAccessForSelectedRole = getAccessForSelectedRole;
exports.updateAccessForSelectedRole = updateAccessForSelectedRole;