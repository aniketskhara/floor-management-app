const express = require("express");
const mongoose = require("mongoose");
const HttpError = require("../models/http-error");
const router = express.Router();
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const stage = require("../config/config");
const MailController = require("./MailController");

const { validationResult } = require("express-validator");

// importing models
const EmployeeModel = require("../models/user");
const RoleManagementModel = require("../models/roleManagement");

const getEmployee = async (req, res, next) => {
  let employee,
    EmployeeCount = 0;

  if (req.query.department) {
    try {
      employee = await EmployeeModel.find({
        department: req.query.department
      }).populate("department");
      EmployeeCount = await EmployeeModel.countDocuments({
        department: req.query.department
      });
    } catch (err) {
      const error = new HttpError("Something went wrong", 500);
      return next(error);
    }
  } else {
    try {
      employee = await EmployeeModel.find().populate("department");
      EmployeeCount = await EmployeeModel.countDocuments(employee);
    } catch (err) {
      const error = new HttpError("Something went wrong", 500);
      return next(error);
    }
  }
  await res.json({ employees: employee, EmployeeCount: EmployeeCount });
};

const createEmployee = async (req, res, next) => {
  const {
    email,
    firstName,
    lastName,
    skype_id,
    contactNumber,
    experience,
    password,
    role,
    department
  } = req.body;

  passFormail = password;

  const employee = new EmployeeModel({
    email,
    firstName,
    lastName,
    skype_id,
    contactNumber,
    experience,
    password,
    role,
    department
  });

  try {
    await employee.save();
    await MailController.sendMailOnBooking(email, passFormail);
  } catch (err) {
    return next(res.status(400).send({ err: err.message }));
  }

  res.status(201).json({ employee: employee });
};

const getEmployeeById = async (req, res, next) => {
  const id = req.params.id;
  let employee;

  try {
    employee = await EmployeeModel.findById(id).populate("department");
  } catch (err) {
    const error = new HttpError("Unable to fetch.", 400);
    return next(error);
  }

  if (!employee) {
    return next(new HttpError("No record found.", 404));
  } else {
    res.json({ employee: employee.toObject({ getters: true }) });
  }
};

const updateEmployee = async (req, res, next) => {
  const id = req.params.id;
  const {
    firstName,
    lastName,
    email,
    skype_id,
    contactNumber,
    experience,
    role,
    department
  } = req.body;

  let updatedEmployee;
  try {
    updatedEmployee = await EmployeeModel.findById(id);
  } catch (err) {
    return next(res.status(400).send({ err: "Employee not found" }));
  }

  updatedEmployee.firstName = firstName;
  updatedEmployee.lastName = lastName;
  updatedEmployee.email = email;
  updatedEmployee.skype_id = skype_id;
  updatedEmployee.contactNumber = contactNumber;
  updatedEmployee.experience = experience;
  updatedEmployee.role = role;
  updatedEmployee.department = department;

  try {
    await updatedEmployee.save();
  } catch (err) {
    return next(res.status(400).send({ err: err.message }));
  }

  res
    .status(200)
    .json({ updatedEmployee: updatedEmployee.toObject({ getters: true }) });
};

const deleteEmployee = async (req, res, next) => {
  const id = req.params.id;
  let employee;
  try {
    employee = await EmployeeModel.findById(id);
  } catch (err) {
    const error = new HttpError("Something went wrong", 500);
    return next(error);
  }

  if (!employee) {
    return next(new HttpError("something went wrong", 500));
  } else {
    try {
      await employee.remove();
      res.json({ message: "Employee deleted successfully" });
    } catch (err) {
      const error = new HttpError("cannot delete the employee", 422);
      return next(error);
    }
  }

  res.status(200).json({ message: "Deleted successfully!" });
};

const Login = async (req, res, next) => {
  let result = {};
  let status = 200;
  const { email, password } = req.body;
  let user;

  try {
    user = await EmployeeModel.findOne({ email }).populate("role");
  } catch (error) {
    return next(res.status(404).send({ err: "Employee not found" }));
  }

  if (user) {
    // We could compare passwords in our model instead of below
    bcrypt
      .compare(password, user.password)
      .then(match => {
        if (match) {
          status = 200;
          // Created a token
          const payload = { user: user.email };
          const options = { expiresIn: "2d" };
          const secret = process.env.JWT_SECRET;
          const token = jwt.sign(payload, secret, options);

          result.token = token;
          result.status = status;
          result.result = user;
          result.user_id = user._id;
        } else {
          status = 401;
          result.status = status;
          result.error = "Authentication error";
        }
        res.status(status).send(result);
      })
      .catch(err => {
        status = 500;
        result.status = status;
        result.error = "";
        res.status(status).send(result);
      });
  } else {
    status = 404;
    result.status = status;
    result.error = "";
    res.status(status).send(result);
  }
};

const UpdatePassword = async (req, res, next) => {
  let result = {};
  const { email, password, new_password } = req.body;
  let user;

  try {
    user = await EmployeeModel.findOne({ email });
  } catch (error) {
    return next(res.status(404).send({ err: "Employee not found" }));
  }

  if (user) {
    // We could compare passwords in our model instead of below
    bcrypt
      .compare(password, user.password)
      .then(match => {
        if (match) {
          user.password = new_password;
          var hash = bcrypt.hashSync(user.password, stage.saltingRounds);
          user.password = hash;
          console.log(user.password);
          if (user.save()) {
            return next(
              res.status(200).send({ message: "Password changed successfully" })
            );
          } else {
            result.status = 400;
            result.result = user;
            result.message = "Unable to change password";
          }
        } else {
          status = 400;
          result.status = status;
          result.error = "Current password did not match";
        }
        res.status(status).send(result);
      })
      .catch(err => {
        status = 400;
        result.status = status;
        result.error = "Current password did not match";
        res.status(status).send(result);
      });
  } else {
    status = 404;
    result.status = status;
    result.error = "User not found";
    res.status(status).send(result);
  }
};

const sendPermissions = async user => {
  let role_id;
  role_id = user.role._id;
  try {
    permissions = await RoleManagementModel.findOne({ role_id: role_id });
  } catch (err) {
    const error = new HttpError("Something went wrong", 500);
    return next(error);
  }
  return permissions.access.toObject({ getters: true });
};

exports.getEmployee = getEmployee;
exports.createEmployee = createEmployee;
exports.getEmployeeById = getEmployeeById;
exports.deleteEmployee = deleteEmployee;
exports.updateEmployee = updateEmployee;
exports.Login = Login;
exports.UpdatePassword = UpdatePassword;
