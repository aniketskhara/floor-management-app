const express = require("express");
const mongoose = require("mongoose");
const HttpError = require("../models/http-error");
const router = express.Router();
const { validationResult } = require("express-validator");

// importing models
const DepartmentModel = require("../models/department");

const getDepartments = async (req, res) => {
  let departments, departmentsCount;
  try {
    departments = await DepartmentModel.find();
    departmentsCount = await DepartmentModel.countDocuments(departments);
  } catch (err) {
    return next(res.status(404).send({ error: err.message }));
  }
  res.json({ departments: departments, departmentsCount: departmentsCount });
};

const createDepartment = async (req, res, next) => {
  const { name } = req.body;

  const department = new DepartmentModel({
    name
  });

  try {
    await department.save();
    res.json({ message: "Department created Successfully" });
  } catch (err) {
    return next(
      res.status(400).send({ error: "Please enter department name" })
    );
  }

  res.status(201).json({ department: department });
};

const getDepartmentById = async (req, res, next) => {
  const id = req.params.id;
  let department;

  try {
    department = await DepartmentModel.findById(id);
  } catch (err) {
    return next(res.status(404).send({ error: err.message }));
  }

  if (!department) {
    return next(res.status(404).send({ error: err.message }));
  } else {
    res.json({ department: department.toObject({ getters: true }) });
  }
};

const updateDepartment = async (req, res, next) => {
  const id = req.params.id;

  const { name } = req.body;
  let updatedDepartment;
  try {
    updatedDepartment = await DepartmentModel.findById(id);
  } catch (err) {
    return next(res.status(404).send({ error: err.message }));
  }

  updatedDepartment.name = name;

  try {
    await updatedDepartment.save();
  } catch {
    return next(res.status(400).send({ error: err.message }));
  }

  res
    .status(200)
    .json({ updatedDepartment: updatedDepartment.toObject({ getters: true }) });
};

const deleteDepartment = async (req, res, next) => {
  const id = req.params.id;

  let deletedDepartment;
  try {
    deletedDepartment = await DepartmentModel.findById(id);
  } catch (err) {
    return next(res.status(404).send({ error: err.message }));
  }

  if (!deletedDepartment) {
    return next(res.status(400).send({ error: err.message }));
  } else {
    try {
      await deletedDepartment.remove();
      res.json({ message: "Department deleted successfully" });
    } catch (err) {
      return next(res.status(400).send({ error: err.message }));
    }
  }
  res.status(200).json({ message: "Deleted successfully!" });
};

exports.getDepartments = getDepartments;
exports.createDepartment = createDepartment;
exports.getDepartmentById = getDepartmentById;
exports.deleteDepartment = deleteDepartment;
exports.updateDepartment = updateDepartment;
