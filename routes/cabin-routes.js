const express = require("express");
const { check } = require("express-validator");
const CabinsController = require("../controllers/cabins");
const HttpError = require("../models/http-error");
const router = express.Router();
const validateToken = require("../utils/utils").validateToken;

const mongoose = require("mongoose");

/**
 * @swagger
 * /cabins:
 *   get:
 *     tags:
 *       - Cabins
 *     description: Returns all cabins
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of cabins
 */
router.get("/", validateToken, CabinsController.getCabins);

/**
 * @swagger
 * /cabins:
 *   post:
 *     tags:
 *       - Cabins
 *     description: Saves an cabin
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/Cabin'
 *           type: object
 *           properties:
 *             cabin_no:
 *               type: string
 *
 *         required:
 *           - firstName
 *     responses:
 *       201:
 *         description: New cabin created successfully
 */
router.post(
  "/",
  validateToken,
  [
    check("cabin_no")
      .not()
      .isEmpty()
  ],
  CabinsController.createCabin
);

/**
 * @swagger
 * /cabins/{id}:
 *   get:
 *     tags:
 *       - Cabins
 *     description: Returns Object of single cabin
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: Cabin's id
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Cabin Object
 */
router.get("/:id", validateToken, CabinsController.getCabinById);

/**
 * @swagger
 * /cabins/{id}:
 *   delete:
 *     tags:
 *       - Cabins
 *     description: Deletes a single cabin
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: Cabin's id
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Cabin Deleted Successfully
 */
router.delete("/:id", validateToken, CabinsController.deleteCabin);

/**
 * @swagger
 * /cabins/{id}:
 *   patch:
 *     tags:
 *       - Cabins
 *     description: Updates a single cabin
 *     produces: application/json
 *     parameters:
 *       - name: id
 *         description: Cabin's id
 *         in: path
 *         required: true
 *         type: string
 *       - name: cabin
 *         in: body
 *         description: Fields for the cabin resource
 *     responses:
 *       200:
 *         description: Cabin updated successfully
 */
router.patch(
  "/:id",
  validateToken,
  [
    check("cabin_no")
      .not()
      .isEmpty()
  ],
  CabinsController.updateCabin
);

module.exports = router;
