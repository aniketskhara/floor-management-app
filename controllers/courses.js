const express = require("express");
const mongoose = require("mongoose");
const HttpError = require("../models/http-error");
const router = express.Router();
const { validationResult } = require("express-validator");

// importing models
const CourseModel = require("../models/course");

const getCourses = async (req, res) => {
  let courses;
  try {
    courses = await CourseModel.find();
  } catch (err) {
    const error = new HttpError("Something went wrong", 500);
    return next(error);
  }
  res.json({ courses: courses });
};

const createCourse = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(new HttpError("Invalid inputs. Please check the inputs", 422));
  }

  const { name, links } = req.body;

  const course = new CourseModel({
    name,
    links
  });

  try {
    await course.save();
    res.json({ message: "Course created Successfully" });
  } catch (err) {
    const error = new HttpError("Cannot complete the action", 422);
    return next(error);
  }

  res.status(201).json({ course: course });
};

const getCourseById = async (req, res, next) => {
  const id = req.params.id;
  let course;

  try {
    course = await CourseModel.findById(id);
  } catch (err) {
    const error = new HttpError("Unable to delete.", 500);
    return next(error);
  }

  if (!course) {
    return next(new HttpError("No record found.", 404));
  } else {
    res.json({ course: course.toObject({ getters: true }) });
  }
};

const updateCourse = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(new HttpError("Invalid inputs. Please check the inputs", 422));
  }

  const id = req.params.id;

  const { links, name } = req.body;

  let updatedCourses;
  try {
    updatedCourses = await CourseModel.findById(id);
  } catch (err) {
    const error = new HttpError("Something went wrong", 500);
    return next(error);
  }

  updatedCourses.links = links;
  updatedCourses.name = name;

  try {
    await updatedCourses.save();
  } catch (err) {
    const error = new HttpError("Something went wrong", 500);
    return next(error);
  }

  res
    .status(200)
    .json({ updatedCourses: updatedCourses.toObject({ getters: true }) });
};

const deleteCourse = async (req, res, next) => {
  const id = req.params.id;

  let deletedCourse;
  try {
    deletedCourse = CourseModel.findById(id);
  } catch (err) {
    const error = new HttpError("Something went wrong", 500);
    return next(error);
  }

  if (!deletedCourse) {
    return next(new HttpError("something went wrong", 500));
  } else {
    try {
      await deletedCourse.remove();
      res.json({ message: "Course deleted successfully" });
    } catch (err) {
      const error = new HttpError("cannot delete the employee", 422);
      return next(error);
    }
  }
};

exports.getCourses = getCourses;
exports.createCourse = createCourse;
exports.getCourseById = getCourseById;
exports.deleteCourse = deleteCourse;
exports.updateCourse = updateCourse;
